<?php
/**
 * @file
 * Reports on users by role
 */
 
function usercount_by_role() {
    $sql = "SELECT COUNT(*) AS count FROM {users} u WHERE u.uid NOT IN (select distinct uid from {users_roles}) and u.uid <> 0"; 
    $data = db_query($sql)->fetchField();
    $rows = array();
    if(variable_get('usercount_devel',0) == 1) {
      $rows[] = array(t('Role ID'), t('Users with no special role'), array('data' => $data, 'align' => 'right') );
    }else{
        $rows[] = array(t('Users with no special role'),            array('data' => $data, 'align' => 'right'));
    }

    $sql = "SELECT r.rid AS rid, r.name AS role, COUNT(*) AS count from {role} r, {users_roles} u WHERE u.rid = r.rid GROUP BY r.rid;";
    $result = db_query($sql);
    foreach ($result AS $data) {
      if(variable_get('usercount_devel',0) == 1) {
        // if devel mode
       
        $rows[] = array(
          array('data' => $data->rid, 'class' => 'col0'),
          array('data' => $data->role, 'class' => 'col1'),
          array('data' => $data->count, 'class' => 'col2', 'align' => 'right')     );
      }else{
        $rows[] = array(
          array('data' => $data->role, 'class' => 'col1'),
          array('data' => $data->count, 'class' => 'col2', 'align' => 'right')     );
      }

    }
    $header = array();

    $variables = array('header' => $header,
                 'rows' => $rows,
                 'attributes' => array('class' => 'usercount'), t('Users by role'),
                 'caption' => '',
                 'colgroups' => array(),
                 'sticky' => array(),
                 'empty' => array(),
                 );
    return  theme_table($variables);
}
