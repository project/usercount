<?php
/* @file
 * Implements comment activity reporting
 */

/**
 * Page callback
 */

function usercount_commenters() {
    $output = '<h2>' . t('Top commenters of all time') . '</h2>';
    drupal_add_css(drupal_get_path('module', 'usercount') . '/usercount.css', array('preprocess' => FALSE));
    $sql = "select u.name, count(*) as count from {comment} c, {users} u
            where c.status = 1
            and c.uid = u.uid
            group by u.name
            order by count desc
            limit 10";
    $result = db_query($sql);
    // dpm($result);
    foreach ($result as $data) {
        // dpm($data);
        if ((!$data->name) && $data->count) $data->name = variable_get('anonymous', 'Anonymous');
        $rows[] = array(
      array('data' => $data->name, 'class' => 'col1'),
      array('data' => $data->count, 'class' => 'col2'),
    );
    }
  $header = array(t('Name'), t('Count'));
  $vars = array('header' => $header, 'rows' => $rows);
  $output .= theme('table', $vars);

    /* for each month the site has been active, print a Top Ten list */
  $output .= '<h2>' . t('Top ten commenters by month') . '</h2>';

  $sql = "select distinct FROM_UNIXTIME(created,'%Y-%m') AS yearmonth
        from {node} order by yearmonth";
    $result = db_query($sql);
    foreach ($result as $data) {
        $output .= _query_usercount_commenters($data->yearmonth);
    }
    return $output;
}

function _query_usercount_commenters($month = NULL) {
    dpm('$month is ' . $month);
    $output = '<h3>' . $month . '</h3>';
    $rows = array();
    $sql = "select u.name, count(*) as count
            from {comment} c, {users} u
            where c.status = 1 and c.uid = u.uid
            and FROM_UNIXTIME(c.created,'%Y-%m') = :month
            group by u.name
            order by count desc
            limit 10";
    $result = db_query($sql, array(':month' => $month));
    dpm($result);
    foreach ($result as $data) {
        if ((!$data->name) && $data->count) $data->name = variable_get('anonymous', 'Anonymous');
        $rows[] = array(
          array('data' => $data->name, 'class' => 'col1'),
          array('data' => $data->count, 'class' => 'col2'),
        );
    }
    if (!count($rows)) return t('No comments posted in') . ' ' . $month . '<br />';
    $header = array(t('Name'), t('Count'));
    $vars = array('header' => $header, 'rows' => $rows, /* array('class' => 'usercount'), t('Top commenters for') . ' ' . $month */ );
    $output .= theme('table', $vars);
    return $output;
}

